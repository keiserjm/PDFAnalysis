#!/usr/bin/env python
# coding: utf-8

# In[1]:


from rake_nltk import Rake
import re

import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer

from string import punctuation

from scipy import spatial
import gensim.downloader as api
import numpy as np

import matplotlib.pyplot as plt


# In[2]:


stop_words = set(stopwords.words('english'))
punctuation = list(punctuation)
lemmatizer = WordNetLemmatizer()


# In[3]:


model = api.load("glove-wiki-gigaword-100") #choose from multiple models https://github.com/RaRe-Technologies/gensim-data


# In[4]:


if 0: # To build a new model from a corpus
    from gensim.models.word2vec import Word2Vec
    import gensim.downloader as api

    corpus = api.load('text8')  # download the corpus and return it opened as an iterable
    model = Word2Vec(corpus)  # train a model from the corpus


# In[5]:


if 0:
    api.info("text8")
    arts = []
    for art in corpus:
        arts.append(art)
    print(arts[100])


# In[6]:


# resl = model.most_similar("car")
resl = model.most_similar("car")

"""
output:

[(u'driver', 0.8273754119873047),
 (u'motorcycle', 0.769528865814209),
 (u'cars', 0.7356342077255249),
 (u'truck', 0.7331641912460327),
 (u'taxi', 0.718338131904602),
 (u'vehicle', 0.7177008390426636),
 (u'racing', 0.6697118878364563),
 (u'automobile', 0.6657308340072632),
 (u'passenger', 0.6377975344657898),
 (u'glider', 0.6374964714050293)]

"""
for res in resl:
    print(res)


# In[18]:


def preprocess(s):
    return [i.lower() for i in s.split()]

def get_vector(s):
    return np.sum(np.array([model[i] for i in preprocess(s) if i in model]), axis=0)


# In[8]:


TXTfile = '../Data/TXTData/goldilocks.txt'


# In[9]:


with open(TXTfile,'r') as txt:
    lines = []
    for line in txt:
        lines.append(line.strip())
#
descs = []
ports = {}
port = ''
for iline,line in enumerate(lines):
    if len(line) > 0:
        if line[0] == '#':
            indices = [i.start() for i in re.finditer(' ', line)]
            d1 = int(line[indices[0]:indices[1]])  # Description label 1
            d2 = line[indices[1]+1:indices[-1]]    # Description text
            d3 = int(line[indices[-1]:])           # Description label 2
            phrase = d2.lower()                    # Description text lemmatized w/o stopwords and punctuation
            indices = [i.start() for i in re.finditer(' ', phrase)]
            d4 = [lemmatizer.lemmatize(w) for w in word_tokenize(phrase) if w not in stop_words and w not in punctuation]
            descs.append((d1,d2,d3,d4))
        else:
            if port == '' or line[:7] == 'Portion':
                port = line
            if port not in ports:
                ports[port] = {}
                ports[port]['raw'] = ''
                ports[port]['prc'] = ''
            else:
                line = line.replace('"','') # \\"')
                ports[port]['raw'] += (line + ' ')
                ports[port]['prc'] += (' '.join([lemmatizer.lemmatize(w) for w in word_tokenize(line.lower()) if w not in stop_words and w not in punctuation]) + ' ')
#                 print(iline,line)
#                 print(' '.join([w for w in word_tokenize(line.lower()) if w not in stop_words and w not in punctuation]))
#                 print()


# In[10]:


for desc in descs:
    print(desc)


# In[11]:


if 0:
    for port in ports:
        print(port)
        for prt in ports[port]:
            print(prt,'\t',ports[port][prt])
        print()


# In[12]:


contents = []
for port in ports:
    for sent in ports[port]['prc'].split('.'):
        contents.append(' '.join([w for w in word_tokenize(sent.lstrip().lower()) if w not in punctuation]))
#
contents.append(' '.join([w for w in word_tokenize('Mark zuckerberg owns the facebook company'.lstrip().lower()) if w not in stop_words and w not in punctuation]))
contents.append(' '.join([w for w in word_tokenize('Facebook company ceo is mark zuckerberg'.lstrip().lower())   if w not in stop_words and w not in punctuation]))
contents.append(' '.join([w for w in word_tokenize('Microsoft is owned by Bill gates'.lstrip().lower())          if w not in stop_words and w not in punctuation]))
contents.append(' '.join([w for w in word_tokenize('How to learn japanese'.lstrip().lower())                     if w not in stop_words and w not in punctuation]))


# In[13]:


ports['Special 1'] = {}
ports['Special 1']['raw'] = ' '.join([w for w in word_tokenize('Mark zuckerberg owns the facebook company'.lstrip().lower()) if w not in stop_words and w not in punctuation])
ports['Special 2'] = {}
ports['Special 2']['raw'] = ' '.join([w for w in word_tokenize('Facebook company ceo is mark zuckerberg'.lstrip().lower())   if w not in stop_words and w not in punctuation])
ports['Special 3'] = {}
ports['Special 3']['raw'] = ' '.join([w for w in word_tokenize('Microsoft is owned by Bill gates'.lstrip().lower())          if w not in stop_words and w not in punctuation])
ports['Special 4'] = {}
ports['Special 4']['raw'] = ' '.join([w for w in word_tokenize('How to learn japanese'.lstrip().lower())                     if w not in stop_words and w not in punctuation])


# In[15]:


depth = 5

scores = {}
for iport,port in enumerate(ports):
    content = ports[port]['raw']
    content = ' '.join([lemmatizer.lemmatize(w) for w in word_tokenize(content.lower()) if w not in stop_words and w not in punctuation])
    print(ports[port]['raw'])
    print(content)
    sumSim = 0
    scores[port] = []
    for idesc,desc in enumerate(descs):
        des = ' '.join(desc[3])
        score = 1 - spatial.distance.cosine(get_vector(des),get_vector(content))
        scores[port].append(score)
        sumSim += score
        # print('\t{3:2d} {0:6.3f}  {2:80s}  {1:s}'.format(score,des,desc[1],idesc))
    # print(' {0:6.3f}  {1:5.3f}'.format(sumSim,sumSim/len(descs)))

    # print(port)
    # print(ports[port]['raw'])
    indx = np.argsort(scores[port])[::-1][:depth]
    # print(indx)
    for i in indx:
        print('{0:2d} {1:6.3f} {2:s}'.format(i,scores[port][i],descs[i][1])) # ,ports[list(ports.keys())[i]])
    
    print()

    if iport == -2:
        break


# In[28]:


s0 = 'The fact that the Bear family uses a tamper-evident seal on their door.'
s1 = 'learn japanese'

s0 = ' '.join([w for w in word_tokenize(s0.lstrip().lower()) if w not in stop_words and w not in punctuation])
s1 = ' '.join([w for w in word_tokenize(s1.lstrip().lower()) if w not in stop_words and w not in punctuation])

print('s0 vs s1 -> {0:6.3f}'.format(1 - spatial.distance.cosine(get_vector(s0), get_vector(s1))))
print('s0 vs s2 -> {0:6.3f}'.format(1 - spatial.distance.cosine(get_vector(s1), get_vector(s0))))


# In[24]:


s0


# In[25]:


s1


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:


fig,axs = plt.subplots(figsize=(30,5))
for port in scores:
    plt.plot(scores[port])
plt.show()


# In[ ]:





# In[ ]:





# In[ ]:


docRaw = ''
docPrc = ''
for port in ports:
    docRaw += ports[port]['raw']
    docPrc += ports[port]['prc']


# In[ ]:


docPrc


# In[ ]:


ans = [w for w in word_tokenize(docPrc) if w not in punctuation]
print(ans)


# In[ ]:


docRaw


# In[ ]:


# Uses stopwords for english from NLTK, and all puntuation characters by default
r = Rake()

text = str(docRaw).lower()

# Extraction given the text.
r.extract_keywords_from_text(text)

# Extraction given the list of strings where each string is a sentence.
# r.extract_keywords_from_sentences(<list of sentences>)

# To get keyword phrases ranked highest to lowest.
# r.get_ranked_phrases()

# To get keyword phrases ranked highest to lowest with scores.
r.get_ranked_phrases_with_scores()


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:




