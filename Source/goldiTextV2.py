#!/usr/bin/env python
# coding: utf-8

# In[ ]:


from rake_nltk import Rake
import re

import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer

from string import punctuation

from scipy import spatial
import gensim.downloader as api
import numpy as np

import matplotlib.pyplot as plt


# In[ ]:


from sentence_transformers import SentenceTransformer, util
import numpy as np
model = SentenceTransformer('stsb-roberta-large')


# In[ ]:


stop_words = set(stopwords.words('english'))
punctuation = list(punctuation)
lemmatizer = WordNetLemmatizer()


# In[ ]:


TXTfile = '../Data/TXTData/goldilocks.txt'


# In[ ]:


with open(TXTfile,'r') as txt:
    lines = []
    for line in txt:
        lines.append(line.strip())
#
descs = []
ports = {}
port = ''
for iline,line in enumerate(lines):
    if len(line) > 0:
        if line[0] == '#':
            indices = [i.start() for i in re.finditer(' ', line)]
            d1 = int(line[indices[0]:indices[1]])  # Description label 1
            d2 = line[indices[1]+1:indices[-1]]    # Description text
            d3 = int(line[indices[-1]:])           # Description label 2
            phrase = d2.lower()                    # Description text lemmatized w/o stopwords and punctuation
            indices = [i.start() for i in re.finditer(' ', phrase)]
            d4 = [lemmatizer.lemmatize(w) for w in word_tokenize(phrase) if w not in stop_words and w not in punctuation]
            descs.append([d1,d2,d3,d4])
        else:
            if port == '' or line[:7] == 'Portion':
                port = line
            if port not in ports:
                ports[port] = {}
                ports[port]['raw'] = ''
                ports[port]['prc'] = ''
            else:
                line = line.replace('"','') # \\"')
                ports[port]['raw'] += (line + ' ')
                ports[port]['prc'] += (' '.join([lemmatizer.lemmatize(w) for w in word_tokenize(line.lower()) if w not in stop_words and w not in punctuation]) + ' ')
#                 print(iline,line)
#                 print(' '.join([w for w in word_tokenize(line.lower()) if w not in stop_words and w not in punctuation]))
#                 print()


# In[ ]:


descList = []
for desc in descs:
    descList.append(desc[1])
descs_embeddings = model.encode(descList, convert_to_tensor=True)


# In[ ]:


top_k = 5
for port in ports:
    print(port)
    portion_score = np.zeros((len(descList)))
    sents = nltk.sent_tokenize(ports[port]['raw']) # .lower())
    for sent in sents:
        sentence_embedding = model.encode(sent, convert_to_tensor=True)
        cos_scores = util.pytorch_cos_sim(sentence_embedding, descs_embeddings)[0]
        top_results = np.argpartition(-cos_scores, range(top_k))[0:top_k]
        portion_score += cos_scores.numpy()

        print("\tSentence:", sent)
        for idx in top_results[0:top_k]:
            print("\t\t{0:8.5f}  {1:s}".format(cos_scores[idx],descList[idx]))
        print()
    print('Average for portion:  ')
    portion_score /= len(sents)
    top_results = np.argpartition(-portion_score, range(top_k))[0:top_k]
    for idx in top_results[0:top_k]:
        print("\t\t{0:8.5f}  {1:s}".format(portion_score[idx],descList[idx]))
    print()
    print()
    
    portion_embedding = model.encode(ports[port]['raw'], convert_to_tensor=True)
    cos_scores = util.pytorch_cos_sim(portion_embedding, descs_embeddings)[0]
    top_results = np.argpartition(-cos_scores, range(top_k))[0:top_k]
    print("Portion Directly:", ports[port]['raw'])
    for idx in top_results[0:top_k]:
        print("\t\t{0:8.5f}  {1:s}".format(cos_scores[idx],descList[idx]))
    print()


# In[ ]:


portList = []
for port in ports:
    portList.append(ports[port]['raw'])
ports_embeddings = model.encode(portList, convert_to_tensor=True)


# In[ ]:


top_k = 5
for port1 in ports:
    port1_embedding = model.encode(ports[port1]['raw'], convert_to_tensor=True)
    
    cos_scores = util.pytorch_cos_sim(port1_embedding, ports_embeddings)[0]
    top_results = np.argpartition(-cos_scores, range(top_k))[0:top_k]

    print("Test Portion:",ports[port1]['raw'])
    for idx in top_results[1:top_k]:
        print("\t{0:8.5f}  {1:s}".format(cos_scores[idx],portList[idx][:150]))
    print()
    


# In[ ]:





# In[ ]:





# In[ ]:


# # Load your usual SpaCy model (one of SpaCy English models)
# import spacy
# nlp = spacy.load('en_core_web_sm')

# # Add neural coref to SpaCy's pipe
# import neuralcoref
# neuralcoref.add_to_pipe(nlp)

# # You're done. You can now use NeuralCoref as you usually manipulate a SpaCy document annotations.
# doc = nlp(u'My sister has a dog. She loves him.')


# In[ ]:




